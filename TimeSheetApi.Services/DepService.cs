﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeSheetApi.Models;

namespace TimeSheetApi.Services
{
    public class DepService : IDepserviseRepository
    {
        public List<Department> all()
        {
            List<Department> departments = new List<Department>();
        
            departments.Add(new Department { DepId = 1, DepName = "IT" });
            departments.Add(new Department { DepId = 2, DepName = "Management" });
            departments.Add(new Department { DepId = 3, DepName = "Marketing" });
       
            return departments;
        }


    }
}
    
