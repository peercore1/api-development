﻿using TimeSheetApi.Models;

namespace TimeSheetApi.Services
{
    public class EmpService : IEmpserviceRepository
    {
        public List<Employee> all()
        {

            {
                List<Employee> employees = new List<Employee>();

                employees.Add(new Employee { Id = 1, Name = "Thilina", Description = "New C# dev team", StatusEmp = EmpStatus.contract});
                employees.Add(new Employee { Id = 2, Name = "Thilina", Description = "New C# dev team", StatusEmp = EmpStatus.permenant });
                employees.Add(new Employee { Id = 3, Name = "Imalka", Description = "New C# dev team", StatusEmp = EmpStatus.temp });

                return employees;

            }
           

        }


    }

}
