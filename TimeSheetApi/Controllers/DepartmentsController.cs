﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using TimeSheetApi.Services;

namespace TimeSheetApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DepartmentsController : ControllerBase
    {
        private readonly IDepserviseRepository _depService;
        public DepartmentsController(IDepserviseRepository depServiceRepo)
        {
            _depService = depServiceRepo;
        }

        [HttpGet("{id?}")]
        public IActionResult GetDepartment(int? id)
        {
            var depdata = _depService.all();
            if(id!=null)
            {
                depdata = depdata.Where(d=> d.DepId == id).ToList();
                return Ok(depdata);
         
            }
            else
            {
                return NotFound();

            }
           

        }

        [HttpGet]
        public IActionResult GetAllDepartments()
        {
    
            {
                var depdata = _depService.all().ToList();
                return Ok(depdata);

            }
    

        }


    }
}
